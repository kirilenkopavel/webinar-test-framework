package ivan.smolin.webinar.apiTests.employee.getEmployee;

import ivan.smolin.webinar.apiTests.employee.EmployeeApiBaseTestClass;
import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertOkResponse;

@DisplayName("API. Employee. GET '/employee/{id}'. Позитивные тесты получения сотрудника по ID")
public final class PositiveTests extends EmployeeApiBaseTestClass {

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. Получение сотрудника по ID")
    public void testSuccess() {
        EmployeeModel model = employeeRepository.getRandomActiveEmployee().toEmployeeModel();
        ResponseEntity<EmployeeModel> responseEntity = getEmployeeService.get(model.getId());
        assertOkResponse(responseEntity, model);
    }
}