package ivan.smolin.webinar.apiTests.account.postAccount;

import ivan.smolin.webinar.api.account.model.AccountModel;
import ivan.smolin.webinar.apiTests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static ivan.smolin.webinar.api.account.constants.AccountConstants.COUNTRY;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.COUNTRY_NAME;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.EXCHANGE_RATES;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.NAME;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.NAME_MAX_LENGTH;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertResponseWithErrorMessages;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.fieldIsRequired;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.listOfObjectsIsRequired;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.maxLength;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.minLength;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.objectIsRequired;

@DisplayName("API. Account. POST '/account'. Негативные тесты создания аккаунта")
public final class NegativeTests extends AccountApiBaseTestClass {

    @Test
    @DisplayName("API. Account. POST '/account'. Все атрибуты тела запроса равны null")
    public void testAttributesAreNullFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY, objectIsRequired(COUNTRY));
            put(EXCHANGE_RATES, listOfObjectsIsRequired(EXCHANGE_RATES));
            put(NAME, fieldIsRequired(NAME));
        }};

        ResponseEntity<String> responseEntity = postAccountService.postError(new AccountModel());
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Account. POST '/account'. Атрибуты тела запроса имеют строки с превышением максимальной длины")
    public void testAttributesExceededMaxLengthFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, maxLength(NAME_MAX_LENGTH));
            put(NAME, maxLength(NAME_MAX_LENGTH));
        }};

        AccountModel requestModel = accountRandom.getRandomModelExceeded();
        ResponseEntity<String> responseEntity = postAccountService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Account. POST '/account'. Атрибуты тела запроса имеют длину строк меньше минимальной длины")
    public void testAttributesLessMinLengthFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, minLength(NAME_MIN_LENGTH));
            put(NAME, minLength(CURRENCY_NAME_MIN_LENGTH));
        }};

        AccountModel requestModel = accountRandom.getRandomModelLessMin();
        ResponseEntity<String> responseEntity = postAccountService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }
}