package ivan.smolin.webinar.api.employee.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public final class EmployeeModel {
    private Integer id;
    private String name;
    private final boolean active = true;
    private List<EmployeeExchangeRateModel> exchangeRates;
    private EmployeeCountryModel country;
    private List<EmployeeExtraModel> extras;

    @JsonProperty("created_at")
    private String createdAt;
}