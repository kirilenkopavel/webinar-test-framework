package ivan.smolin.webinar.api.employee.entity;

import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "employee")
public final class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private boolean active;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private List<EmployeeExchangeRateEntity> exchangeRates;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_country_id")
    private EmployeeCountryEntity country;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "employee_extra_map",
            joinColumns = {@JoinColumn(name = "employee_id")},
            inverseJoinColumns = {@JoinColumn(name = "extra_id")})
    private List<EmployeeExtraEntity> extras;

    public EmployeeModel toEmployeeModel() {
        return new EmployeeModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString())
                .setCountry(this.country.toEmployeeCountryModel())
                .setExchangeRates(this.exchangeRates
                        .stream()
                        .map(EmployeeExchangeRateEntity::toEmployeeExchangeRateModel)
                        .collect(Collectors.toList())
                )
                .setExtras(this.extras
                        .stream()
                        .map(EmployeeExtraEntity::toEmployeeExtraModel)
                        .collect(Collectors.toList())
                );
    }
}