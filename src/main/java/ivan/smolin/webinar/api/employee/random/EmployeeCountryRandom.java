package ivan.smolin.webinar.api.employee.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.employee.entity.EmployeeCountryEntity;
import ivan.smolin.webinar.api.employee.model.EmployeeCountryModel;
import org.springframework.stereotype.Component;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.NAME_MAX_LENGTH;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.NAME_MIN_LENGTH;

@Component
public final class EmployeeCountryRandom extends CommonRandom {

    /**
     * Метод для генерации Employee Country Entity
     */
    public EmployeeCountryEntity getRandomEntity() {
        return new EmployeeCountryEntity()
                .setName(faker.name().name())
                .setCreatedAt(getDate());
    }

    /**
     * Методы для генерации Employee Country Model
     */
    public EmployeeCountryModel getRandomModelMax() {
        return new EmployeeCountryModel()
                .setName(getString(NAME_MAX_LENGTH))
                .setCreatedAt(getStringDate());
    }

    public EmployeeCountryModel getRandomModelMin() {
        return new EmployeeCountryModel()
                .setName(getString(NAME_MIN_LENGTH))
                .setCreatedAt(getStringDate());
    }

    public EmployeeCountryModel getRandomModelExceeded() {
        return new EmployeeCountryModel()
                .setName(getString(NAME_MAX_LENGTH + MIN_NUMBER))
                .setCreatedAt(getStringDate());
    }

    public EmployeeCountryModel getRandomModelLessMin() {
        return new EmployeeCountryModel()
                .setName(getString(NAME_MIN_LENGTH - MIN_NUMBER))
                .setCreatedAt(getStringDate());
    }
}