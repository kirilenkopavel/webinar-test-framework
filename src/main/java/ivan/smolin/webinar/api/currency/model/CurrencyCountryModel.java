package ivan.smolin.webinar.api.currency.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class CurrencyCountryModel {
    private Integer id;
    private String name;

    @JsonProperty("created_at")
    private String createdAt;
}