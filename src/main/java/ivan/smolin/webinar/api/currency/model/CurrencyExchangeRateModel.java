package ivan.smolin.webinar.api.currency.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class CurrencyExchangeRateModel {
    private Integer id;
    private Double rate;

    @JsonProperty("rate_date")
    private String rateDate;

    @JsonProperty("created_at")
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public Double getRate() {
        return rate;
    }

    public String getRateDate() {
        return rateDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}